/*
 * PSX_mixer.h
 */

#ifndef _PSX_MIXER_H_
#define _PSX_MIXER_H_

#include <psx.h>
#include "SDL_mixer.h"

struct _Mix_Music {
    Mix_MusicType type;
    union {

        // bj added this since the PSX uses VAG format, possibly raw data
        uint8_t *raw_data;

    } data;
    Mix_Fading fading;
    int fade_step;
    int fade_steps;
    int error;
};


/* Get any errors that have been set */
char *Mix_GetError(void);

/* Open the mixer with a certain audio format */
int Mix_OpenAudio(int frequency, Uint16 format, int channels, int chunksize);

/* Load a RAW, VAG, or WAV file */
Mix_Music *Mix_LoadMUS(const char *filename);

/* Play an opened music */
int Mix_PlayMusic(Mix_Music *music, int loops);

/* Stop music if playing */
int Mix_HaltMusic(void);

/* Pause any music if playing */
void Mix_PauseMusic(void);

/* Open a RAW 'chunk' */
Mix_Chunk *Mix_LoadWAV(char *file);

/* Play an opened 'chunk' */
int Mix_PlayChannel(int channel, Mix_Chunk *chunk, int loops);

#endif // _PSX_MIXER_H_


