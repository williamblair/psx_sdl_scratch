#include "PSX_SDL.h"

/*
 * Keep track of all of the surfaces allocated
 */
uint32_t sdl_surface_count = 0;
SpriteKey sdl_sprites[MAX_SPRITES];

/*
 * Keep track of where stuff's been uploaded to vram
 */
uint32_t vram_tex_x = 0;
uint32_t vram_tex_y = 0;
uint8_t vram_tpage_x = 0; // relative texture page (e.g. 7,0)
uint8_t vram_tpage_y = 0;

uint32_t vram_clut_x = 0;
uint32_t vram_clut_y = 481; // we'll try placing the clut BELOW the display area

static char *bj_debugprint = "Hello World!";

void SDL_DebugPrint(char *message)
{
    bj_debugprint = message;
}

int SDL_Flip(SDL_Surface *screen)
{
    //int i;
    //int x, y, r, g, b;
    if (display_is_old)
    {

        /* Print a debug message */
        /*if (bj_debugprint) {
            GsPrintFont(32, 220, bj_debugprint);
        }*/
        
        GsDrawList();
        while(GsIsDrawing());

        /* Set flag to prevent drawing until image
         * has been set on TV */
        display_is_old = 0;

        /* Switch the drawing and display areas */
		if (S_HEIGHT != 480) {
        	GsSetDispEnvSimple(0, dbuf ? 0 : 240);
        	GsSetDrawEnvSimple(0, dbuf ? 240 : 0, 320, 240);

			/* Switch display buffers */
        	dbuf = !dbuf;
		} else {
			/* For 480 mode display and draw area are the same */
			GsSetDispEnvSimple(0, 0);
        	GsSetDrawEnvSimple(0, 0, S_WIDTH, S_HEIGHT);
		}

		// bj test...
		//GsSortCls(0,0,0);
    }
    
    return 0;
}

/* Map from Palette to Palette */
static Uint8 *Map1to1(SDL_Palette *src, SDL_Palette *dst, int *identical)
{
	Uint8 *map;
	int i;

	if ( identical ) {
		if ( src->ncolors <= dst->ncolors ) {
			/* If an identical palette, no need to map */
			if ( SDL_memcmp(src->colors, dst->colors, src->ncolors*
						sizeof(SDL_Color)) == 0 ) {
				*identical = 1;
				return(NULL);
			}
		}
		*identical = 0;
	}
	map = (Uint8 *)SDL_malloc(src->ncolors);
	if ( map == NULL ) {
		SDL_OutOfMemory();
		return(NULL);
	}
	for ( i=0; i<src->ncolors; ++i ) {
		map[i] = SDL_FindColor(dst,
			src->colors[i].r, src->colors[i].g, src->colors[i].b);
	}
	return(map);
}
/* Map from Palette to BitField */
static Uint8 *Map1toN(SDL_PixelFormat *src, SDL_PixelFormat *dst)
{
	Uint8 *map;
	int i;
	int  bpp;
	unsigned alpha;
	SDL_Palette *pal = src->palette;

	bpp = ((dst->BytesPerPixel == 3) ? 4 : dst->BytesPerPixel);
	map = (Uint8 *)SDL_malloc(pal->ncolors*bpp);
	if ( map == NULL ) {
		SDL_OutOfMemory();
		return(NULL);
	}

	alpha = dst->Amask ? src->alpha : 0;
	/* We memory copy to the pixel map so the endianness is preserved */
	for ( i=0; i<pal->ncolors; ++i ) {
		ASSEMBLE_RGBA(&map[i*bpp], dst->BytesPerPixel, dst,
			      pal->colors[i].r, pal->colors[i].g,
			      pal->colors[i].b, alpha);
	}
	return(map);
}

/* Map from BitField to Dithered-Palette to Palette */
static Uint8 *MapNto1(SDL_PixelFormat *src, SDL_PixelFormat *dst, int *identical)
{
	/* Generate a 256 color dither palette */
	SDL_Palette dithered;
	SDL_Color colors[256];
	SDL_Palette *pal = dst->palette;

	/* SDL_DitherColors does not initialize the 'unused' component of colors,
	   but Map1to1 compares it against pal, so we should initialize it. */
	SDL_memset(colors, 0, sizeof(colors));

	dithered.ncolors = 256;
	SDL_DitherColors(colors, 8);
	dithered.colors = colors;
	return(Map1to1(&dithered, pal, identical));
}

int SDL_MapSurface (SDL_Surface *src, SDL_Surface *dst)
{
	SDL_PixelFormat *srcfmt;
	SDL_PixelFormat *dstfmt;
	SDL_BlitMap *map;

	/* Clear out any previous mapping */
	map = src->map;
	/*if ( (src->flags & SDL_RLEACCEL) == SDL_RLEACCEL ) {
		SDL_UnRLESurface(src, 1);
	}*/
	SDL_InvalidateMap(map);

	/* Figure out what kind of mapping we're doing */
	map->identity = 0;
	srcfmt = src->format;
	dstfmt = dst->format;
	switch (srcfmt->BytesPerPixel) {
	    case 1:
		switch (dstfmt->BytesPerPixel) {
		    case 1:
			/* Palette --> Palette */
			/* If both SDL_HWSURFACE, assume have same palette */
			if ( ((src->flags & SDL_HWSURFACE) == SDL_HWSURFACE) &&
			     ((dst->flags & SDL_HWSURFACE) == SDL_HWSURFACE) ) {
				map->identity = 1;
			} else {
				map->table = Map1to1(srcfmt->palette,
					dstfmt->palette, &map->identity);
			}
			if ( ! map->identity ) {
				if ( map->table == NULL ) {
					return(-1);
				}
			}
			if (srcfmt->BitsPerPixel!=dstfmt->BitsPerPixel)
				map->identity = 0;
			break;

		    default:
			/* Palette --> BitField */
			map->table = Map1toN(srcfmt, dstfmt);
			if ( map->table == NULL ) {
				return(-1);
			}
			break;
		}
		break;
	default:
		switch (dstfmt->BytesPerPixel) {
		    case 1:
			/* BitField --> Palette */
			map->table = MapNto1(srcfmt, dstfmt, &map->identity);
			if ( ! map->identity ) {
				if ( map->table == NULL ) {
					return(-1);
				}
			}
			map->identity = 0;	/* Don't optimize to copy */
			break;
		    default:
			/* BitField --> BitField */
			if ( FORMAT_EQUAL(srcfmt, dstfmt) )
				map->identity = 1;
			break;
		}
		break;
	}

	map->dst = dst;
	map->format_version = dst->format_version;

	/* Choose your blitters wisely */
	return(SDL_CalculateBlit(src));
}

/*
 * Change any previous mappings from/to the new surface format
 */
void SDL_FormatChanged(SDL_Surface *surface)
{
	static int format_version = 0;
	++format_version;
	if ( format_version < 0 ) { /* It wrapped... */
		format_version = 1;
	}
	surface->format_version = format_version;
	SDL_InvalidateMap(surface->map);
}

/*
 * Free a previously allocated format structure
 */
void SDL_FreeFormat(SDL_PixelFormat *format)
{
	if ( format ) {
		if ( format->palette ) {
			if ( format->palette->colors ) {
				SDL_free(format->palette->colors);
			}
			SDL_free(format->palette);
		}
		SDL_free(format);
	}
}

/*
 * Calculate the pad-aligned scanline width of a surface
 */
Uint16 SDL_CalculatePitch(SDL_Surface *surface)
{
	Uint16 pitch;

	/* Surface should be 4-byte aligned for speed */
	pitch = surface->w*surface->format->BytesPerPixel;
	switch (surface->format->BitsPerPixel) {
		case 1:
			pitch = (pitch+7)/8;
			break;
		case 4:
			pitch = (pitch+1)/2;
			break;
		default:
			break;
	}
	pitch = (pitch + 3) & ~3;	/* 4-byte aligning */
	return(pitch);
}


/*
 * Allocate a pixel format structure and fill it according to the given info.
 */
SDL_PixelFormat *SDL_AllocFormat(int bpp,
			Uint32 Rmask, Uint32 Gmask, Uint32 Bmask, Uint32 Amask)
{
	SDL_PixelFormat *format;
	Uint32 mask;

	/* Allocate an empty pixel format structure */
	format = SDL_malloc(sizeof(*format));
	if ( format == NULL ) {
		SDL_OutOfMemory();
		return(NULL);
	}
	SDL_memset(format, 0, sizeof(*format));
	format->alpha = SDL_ALPHA_OPAQUE;

	/* Set up the format */
	format->BitsPerPixel = bpp;
	format->BytesPerPixel = (bpp+7)/8;
	if ( Rmask || Bmask || Gmask ) { /* Packed pixels with custom mask */
		format->palette = NULL;
		format->Rshift = 0;
		format->Rloss = 8;
		if ( Rmask ) {
			for ( mask = Rmask; !(mask&0x01); mask >>= 1 )
				++format->Rshift;
			for ( ; (mask&0x01); mask >>= 1 )
				--format->Rloss;
		}
		format->Gshift = 0;
		format->Gloss = 8;
		if ( Gmask ) {
			for ( mask = Gmask; !(mask&0x01); mask >>= 1 )
				++format->Gshift;
			for ( ; (mask&0x01); mask >>= 1 )
				--format->Gloss;
		}
		format->Bshift = 0;
		format->Bloss = 8;
		if ( Bmask ) {
			for ( mask = Bmask; !(mask&0x01); mask >>= 1 )
				++format->Bshift;
			for ( ; (mask&0x01); mask >>= 1 )
				--format->Bloss;
		}
		format->Ashift = 0;
		format->Aloss = 8;
		if ( Amask ) {
			for ( mask = Amask; !(mask&0x01); mask >>= 1 )
				++format->Ashift;
			for ( ; (mask&0x01); mask >>= 1 )
				--format->Aloss;
		}
		format->Rmask = Rmask;
		format->Gmask = Gmask;
		format->Bmask = Bmask;
		format->Amask = Amask;
	} else if ( bpp > 8 ) {		/* Packed pixels with standard mask */
		/* R-G-B */
		if ( bpp > 24 )
			bpp = 24;
		format->Rloss = 8-(bpp/3);
		format->Gloss = 8-(bpp/3)-(bpp%3);
		format->Bloss = 8-(bpp/3);
		format->Rshift = ((bpp/3)+(bpp%3))+(bpp/3);
		format->Gshift = (bpp/3);
		format->Bshift = 0;
		format->Rmask = ((0xFF>>format->Rloss)<<format->Rshift);
		format->Gmask = ((0xFF>>format->Gloss)<<format->Gshift);
		format->Bmask = ((0xFF>>format->Bloss)<<format->Bshift);
	} else {
		/* Palettized formats have no mask info */
		format->Rloss = 8;
		format->Gloss = 8;
		format->Bloss = 8;
		format->Aloss = 8;
		format->Rshift = 0;
		format->Gshift = 0;
		format->Bshift = 0;
		format->Ashift = 0;
		format->Rmask = 0;
		format->Gmask = 0;
		format->Bmask = 0;
		format->Amask = 0;
	}
	if ( bpp <= 8 ) {			/* Palettized mode */
		int ncolors = 1<<bpp;
#ifdef DEBUG_PALETTE
		fprintf(stderr,"bpp=%d ncolors=%d\n",bpp,ncolors);
#endif
		format->palette = (SDL_Palette *)SDL_malloc(sizeof(SDL_Palette));
		if ( format->palette == NULL ) {
			SDL_FreeFormat(format);
			SDL_OutOfMemory();
			return(NULL);
		}
		(format->palette)->ncolors = ncolors;
		(format->palette)->colors = (SDL_Color *)SDL_malloc(
				(format->palette)->ncolors*sizeof(SDL_Color));
		if ( (format->palette)->colors == NULL ) {
			SDL_FreeFormat(format);
			SDL_OutOfMemory();
			return(NULL);
		}
		if ( Rmask || Bmask || Gmask ) {
			/* create palette according to masks */
			int i;
			int Rm=0,Gm=0,Bm=0;
			int Rw=0,Gw=0,Bw=0;
#ifdef ENABLE_PALETTE_ALPHA
			int Am=0,Aw=0;
#endif
			if(Rmask)
			{
				Rw=8-format->Rloss;
				for(i=format->Rloss;i>0;i-=Rw)
					Rm|=1<<i;
			}
#ifdef DEBUG_PALETTE
			fprintf(stderr,"Rw=%d Rm=0x%02X\n",Rw,Rm);
#endif
			if(Gmask)
			{
				Gw=8-format->Gloss;
				for(i=format->Gloss;i>0;i-=Gw)
					Gm|=1<<i;
			}
#ifdef DEBUG_PALETTE
			fprintf(stderr,"Gw=%d Gm=0x%02X\n",Gw,Gm);
#endif
			if(Bmask)
			{
				Bw=8-format->Bloss;
				for(i=format->Bloss;i>0;i-=Bw)
					Bm|=1<<i;
			}
#ifdef DEBUG_PALETTE
			fprintf(stderr,"Bw=%d Bm=0x%02X\n",Bw,Bm);
#endif
#ifdef ENABLE_PALETTE_ALPHA
			if(Amask)
			{
				Aw=8-format->Aloss;
				for(i=format->Aloss;i>0;i-=Aw)
					Am|=1<<i;
			}
# ifdef DEBUG_PALETTE
			fprintf(stderr,"Aw=%d Am=0x%02X\n",Aw,Am);
# endif
#endif
			for(i=0; i < ncolors; ++i) {
				int r,g,b;
				r=(i&Rmask)>>format->Rshift;
				r=(r<<format->Rloss)|((r*Rm)>>Rw);
				format->palette->colors[i].r=r;

				g=(i&Gmask)>>format->Gshift;
				g=(g<<format->Gloss)|((g*Gm)>>Gw);
				format->palette->colors[i].g=g;

				b=(i&Bmask)>>format->Bshift;
				b=(b<<format->Bloss)|((b*Bm)>>Bw);
				format->palette->colors[i].b=b;

#ifdef ENABLE_PALETTE_ALPHA
				a=(i&Amask)>>format->Ashift;
				a=(a<<format->Aloss)|((a*Am)>>Aw);
				format->palette->colors[i].unused=a;
#else
				format->palette->colors[i].unused=0;
#endif
			}
		} else if ( ncolors == 2 ) {
			/* Create a black and white bitmap palette */
			format->palette->colors[0].r = 0xFF;
			format->palette->colors[0].g = 0xFF;
			format->palette->colors[0].b = 0xFF;
			format->palette->colors[1].r = 0x00;
			format->palette->colors[1].g = 0x00;
			format->palette->colors[1].b = 0x00;
		} else {
			/* Create an empty palette */
			SDL_memset((format->palette)->colors, 0,
				(format->palette)->ncolors*sizeof(SDL_Color));
		}
	}
	return(format);
}

/*
 * Create an empty RGB surface of the appropriate depth
 */
SDL_Surface * SDL_CreateRGBSurface (Uint32 flags,
			int width, int height, int depth,
			Uint32 Rmask, Uint32 Gmask, Uint32 Bmask, Uint32 Amask)
{
	SDL_Surface *surface;

	/* Allocate the surface */
	surface = (SDL_Surface *)SDL_malloc(sizeof(*surface));
	if (surface == NULL) {
		SDL_DebugPrint("SDL_CreateRGBSurface: out of memory!\n");
		return NULL;
	}

	surface->flags = SDL_SWSURFACE;

	surface->format = SDL_AllocFormat(depth, Rmask, Gmask, Bmask, Amask);
	if (surface->format == NULL) {
		SDL_free(surface);
		return NULL;
	}

	if (Amask) {
		surface->flags |= SDL_SRCALPHA;
	}
	surface->w = width;
	surface->h = height;
	surface->pitch = SDL_CalculatePitch(surface);
	surface->pixels = NULL;
	surface->offset = 0;
	surface->hwdata = NULL;
	surface->locked = 0;
	surface->map = NULL;
	surface->unused1 = 0;
	SDL_SetClipRect(surface, NULL);
	SDL_FormatChanged(surface);

	/* This code ignores an `if` logic checking for hardware surface,
	 * see the SDL_surface.c version of this function for details
	 */
	surface->pixels = SDL_malloc(surface->h * surface->pitch);
	if (surface->pixels == NULL) {
		SDL_FreeSurface(surface);
		SDL_OutOfMemory();
		return NULL;
	}

	/* This is important for bitmaps */
	SDL_memset(surface->pixels, 0, surface->h*surface->pitch);

	/* Allocate an empty mapping */
	surface->map = SDL_AllocBlitMap();
	if (surface->map == NULL) {
		SDL_FreeSurface(surface);
		return NULL;
	}

	/* The surface is ready to go */
	surface->refcount = 1;
#ifdef CHECK_LEAKS
	++surfaces_allocated;
#endif

	return surface;
}

/*
 * Free a surface created by the above function.
 */
void SDL_FreeSurface (SDL_Surface *surface)
{
	/* Free anything that's not NULL, and not the screen surface */
	/*if ((surface == NULL) ||
	    (current_video &&
	    ((surface == SDL_ShadowSurface)||(surface == SDL_VideoSurface)))) {
		return;
	}*/
	if (surface == NULL) {
		return;
	}
	if ( --surface->refcount > 0 ) {
		return;
	}
	while ( surface->locked > 0 ) {
		SDL_UnlockSurface(surface);
	}
	/*if ( (surface->flags & SDL_RLEACCEL) == SDL_RLEACCEL ) {
	        SDL_UnRLESurface(surface, 0);
	}*/
	if ( surface->format ) {
		SDL_FreeFormat(surface->format);
		surface->format = NULL;
	}
	if ( surface->map != NULL ) {
		SDL_FreeBlitMap(surface->map);
		surface->map = NULL;
	}
	/*if ( surface->hwdata ) {
		SDL_VideoDevice *video = current_video;
		SDL_VideoDevice *this  = current_video;
		video->FreeHWSurface(this, surface);
	}*/
	if ( surface->pixels &&
	     ((surface->flags & SDL_PREALLOC) != SDL_PREALLOC) ) {
		SDL_free(surface->pixels);
	}
	SDL_free(surface);
#ifdef CHECK_LEAKS
	--surfaces_allocated;
#endif
}

/*
 * Calculate an 8-bit (3 red, 3 green, 2 blue) dithered palette of colors
 */
void SDL_DitherColors(SDL_Color *colors, int bpp)
{
	int i;
	if(bpp != 8)
		return;		/* only 8bpp supported right now */

	for(i = 0; i < 256; i++) {
		int r, g, b;
		/* map each bit field to the full [0, 255] interval,
		   so 0 is mapped to (0, 0, 0) and 255 to (255, 255, 255) */
		r = i & 0xe0;
		r |= r >> 3 | r >> 6;
		colors[i].r = r;
		g = (i << 3) & 0xe0;
		g |= g >> 3 | g >> 6;
		colors[i].g = g;
		b = i & 0x3;
		b |= b << 2;
		b |= b << 4;
		colors[i].b = b;
	}
}

/*
 * Lock a surface to directly access the pixels
 */
int SDL_LockSurface (SDL_Surface *surface)
{
	if ( ! surface->locked ) {
		/* Perform the lock */
		/*if ( surface->flags & (SDL_HWSURFACE|SDL_ASYNCBLIT) ) {
			SDL_VideoDevice *video = current_video;
			SDL_VideoDevice *this  = current_video;
			if ( video->LockHWSurface(this, surface) < 0 ) {
				return(-1);
			}
		}*/
		//if ( surface->flags & SDL_RLEACCEL ) {
		//	SDL_UnRLESurface(surface, 1);
		//	surface->flags |= SDL_RLEACCEL;	/* save accel'd state */
		//}
		/* This needs to be done here in case pixels changes value */
		surface->pixels = (Uint8 *)surface->pixels + surface->offset;
	}

	/* Increment the surface lock count, for recursive locks */
	++surface->locked;

	/* Ready to go.. */
	return(0);
}
/*
 * Unlock a previously locked surface
 */
void SDL_UnlockSurface (SDL_Surface *surface)
{
	/* Only perform an unlock if we are locked */
	if ( ! surface->locked || (--surface->locked > 0) ) {
		return;
	}

	/* Perform the unlock */
	surface->pixels = (Uint8 *)surface->pixels - surface->offset;

	/* Unlock hardware or accelerated surfaces */
	/*if ( surface->flags & (SDL_HWSURFACE|SDL_ASYNCBLIT) ) {
		SDL_VideoDevice *video = current_video;
		SDL_VideoDevice *this  = current_video;
		video->UnlockHWSurface(this, surface);
	} else {*/
		/* Update RLE encoded surface with new data */
		//if ( (surface->flags & SDL_RLEACCEL) == SDL_RLEACCEL ) {
		//        surface->flags &= ~SDL_RLEACCEL; /* stop lying */
		//	SDL_RLESurface(surface);
		//}
	/*}*/
}
