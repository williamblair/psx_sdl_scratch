#include "PSX_SDL.h"

/* Compression encodings for BMP files */
#ifndef BI_RGB
#define BI_RGB          0
#define BI_RLE8         1
#define BI_RLE4         2
#define BI_BITFIELDS    3
#endif

/* Max texture width of a sprite */
#define MAX_SPR_SIZE 255

typedef struct {

    uint16_t header; // the header field
    uint32_t filesize; // size of the file in bytes

    uint32_t reserved; // 4 bytes of reserved data (depending on the image that creates it)

    uint32_t pix_offset; // 4 bytes  offset (i.e. starting address) of where the pixmap can be found

} BMP_header;

/*
 * Load BMP (assumes is 16bit)
 */
static void load_bmp(SDL_RWops *src, uint16_t *buffer, int width, int height)
{
    int i,j;
    uint8_t r,g,b;
    uint16_t pixel_buffer[(MAX_SPR_SIZE+1)*S_HEIGHT]; // holds the pixels from the bmp, before we process them
    BMP_header header;

    uint32_t image_width = width, image_height = height;
    uint16_t bits_per_pixel = 16; // assuming this function is only called when the bmp is 16bpp
    uint32_t bytes_read;
    uint32_t padded_width; // the image width aligned/padded to the nearest 4 bytes

    /* Read in the BMP header */
    SDL_RWread(src, &header.header, 2, 1); // Read in the header
    SDL_RWread(src, &header.filesize, 4, 1); // Read in the file size
    SDL_RWread(src, &header.reserved, 4, 1); // Read in reserved
    SDL_RWread(src, &header.pix_offset, 4, 1); // Read in the pixel offset

    /* Go straight to the pixels */
    SDL_RWseek(src, header.pix_offset, RW_SEEK_SET);

    /* when reading, we might need to adjust width with 4byte aligned padding
     * e.g. the rowsize equation from here:
     *   https://en.wikipedia.org/wiki/BMP_file_format
     * align to 2 instead of 4 since uint16 = 2 bytes each
     */
    padded_width = image_width+(image_width%2);
    printf("Padded width: %d\n", padded_width);

    if (bits_per_pixel != 16) {
        printf("!!!!!UNIMPLEMENTED BITS PER PIXEL!!!!!\n");
        printf("  Bits Per Pixel: %d\n", bits_per_pixel);
        return;
    }

    /* read in the pixel data */
    bytes_read = SDL_RWread(src, pixel_buffer, 2, padded_width*image_height);
    printf("Read %d bytes\n", bytes_read);

    /* now we need to flip Y, and swap the R and B components */
    for (i = 0; i < image_width; ++i) {
        for (j = 0; j < image_height; ++j) {

            /* get the pixel at the flipped y coordinate */
            uint16_t curpix = pixel_buffer[(image_height-1-j)*padded_width+i];

            b = curpix & 31;
            g = (curpix>>5)&31;
            r = (curpix>>10)&31;
    
            /* set the new color (switch b and r)
             * also y is flipped compared to curpix
             */
            buffer[j*image_width+i] = (b<<10) | (g<<5) | r;
        }
    }
}

/*
 * This function requires:
 *
 *      SDL_RWFromFile() -- ADDED !
 *      SDL_RWtell()     -- ADDED !
 *      SDL_ClearError() (ignoring)
 *      SDL_RWread()     -- ADDED !
 *      SDL_RWseek()     -- ADDED !
 *      SDL_RWclose()    -- ADDED !
 *      SDL_strncmp()    -- ADDED !
 *      SDL_strcmp()     -- ADDED !
 *      SDL_SetError()   (ignoring)
 *      SDL_ReadLE32()   -- ADDED !
 *      SDL_ReadLE16()   -- ADDED !
 *      SDL_FreeSurface()
 *
 *      SDL_BIG_ENDIAN defined?? (maybe) -- PSX is little endian so probably not
 *
 *      SDL_CreateRGBSurface() -- ADDED !
 *
 *
 */
SDL_Surface *SDL_LoadBMP_RW(SDL_RWops *src, int freesrc)
{
    SDL_bool was_error;
    long fp_offset = 0;
    int bmpPitch;
    int i, j, pad;
    SDL_Surface *surface;
    Uint32 Rmask;
    Uint32 Gmask;
    Uint32 Bmask;
    SDL_Palette *palette;
    Uint8 *bits;
    Uint8 *top, *end;
    SDL_bool topDown;
    int ExpandBMP;
    
    Uint8 *bj_buff = NULL;
    Uint16 bj_buff_size = 0;
    Uint16 bj_buff_index = 0;

    /* The Win32 BMP file header (14 bytes) */
    char   magic[2];
    Uint32 bfSize;
    Uint16 bfReserved1;
    Uint16 bfReserved2;
    Uint32 bfOffBits;

    /* The Win32 BITMAPINFOHEADER struct (40 bytes) */
    Uint32 biSize;
    Sint32 biWidth;
    Sint32 biHeight;
    Uint16 biPlanes;
    Uint16 biBitCount;
    Uint32 biCompression;
    Uint32 biSizeImage;
    Sint32 biXPelsPerMeter;
    Sint32 biYPelsPerMeter;
    Uint32 biClrUsed;
    Uint32 biClrImportant;

    /* Make sure we are passed a valid data source */
    surface = NULL;
    was_error = SDL_FALSE;
    if ( src == NULL ) {
        was_error = SDL_TRUE;
        goto done;
    }
    
    fp_offset = SDL_RWtell(src);
    
    /* Read the file into a buffer */
    SDL_RWseek(src, 0, RW_SEEK_END);
    bj_buff_size = SDL_RWtell(src);
    SDL_RWseek(src, 0, RW_SEEK_SET);
    bj_buff = (Uint8*)SDL_malloc(bj_buff_size);
    if (SDL_RWread(src, bj_buff, 1, bj_buff_size) != bj_buff_size) {
        SDL_DebugPrint("Failed to read into bj buffer!");
        SDL_free(bj_buff);
        was_error = SDL_TRUE;
        goto done;
    }
    
    /* Move back to the beginning of the file for bj 16bmp load */
    SDL_RWseek(src, 0, RW_SEEK_SET);

    /* Read in the BMP file header */
    //fp_offset = SDL_RWtell(src);
    bj_buff_index = fp_offset;
    //SDL_ClearError();
    //if ( SDL_RWread(src, magic, 1, 2) != 2 ) {
    memcpy(magic, &bj_buff[bj_buff_index], 2);
    bj_buff_index += 2;
    //SDL_Error(SDL_EFREAD);
    //was_error = SDL_TRUE;
    //goto done;
    //}
    if ( SDL_strncmp(magic, "BM", 2) != 0 ) {
        //SDL_SetError("File is not a Windows BMP file");
        SDL_DebugPrint("File is now a Windows BMP file");
        was_error = SDL_TRUE;
        goto done;
    }
    memcpy(&bfSize, &bj_buff[bj_buff_index], 4);
    bj_buff_index += 4;
        //bfSize                = SDL_ReadLE32(src);
        
    memcpy(&bfReserved1, &bj_buff[bj_buff_index], 2);
    bj_buff_index += 2;
    //bfReserved1       = SDL_ReadLE16(src);
        
    memcpy(&bfReserved2, &bj_buff[bj_buff_index], 2);
    bj_buff_index += 2;
    //bfReserved2       = SDL_ReadLE16(src);
        
    memcpy(&bfOffBits, &bj_buff[bj_buff_index], 4);
    bj_buff_index += 4;
    //bfOffBits = SDL_ReadLE32(src);

        /* Read the Win32 BITMAPINFOHEADER */
        memcpy(&biSize, &bj_buff[bj_buff_index], 4);
    bj_buff_index += 4;
    //biSize            = SDL_ReadLE32(src);
        if ( biSize == 12 ) {
                biWidth = (Uint32)((bj_buff[bj_buff_index]<<8) | bj_buff[bj_buff_index+1]);
        bj_buff_index += 2;
        //biWidth               = (Uint32)SDL_ReadLE16(src);
                
        biHeight = (Uint32)((bj_buff[bj_buff_index]<<8) | bj_buff[bj_buff_index+1]);
        bj_buff_index += 2;
        //biHeight      = (Uint32)SDL_ReadLE16(src);
                
        memcpy(&biPlanes, &bj_buff[bj_buff_index], 2);
        bj_buff_index += 2;
        //biPlanes      = SDL_ReadLE16(src);
                
        memcpy(&biBitCount, &bj_buff[bj_buff_index], 2);
        bj_buff_index += 2;
        //biBitCount    = SDL_ReadLE16(src);
                biCompression   = BI_RGB;
                biSizeImage     = 0;
                biXPelsPerMeter = 0;
                biYPelsPerMeter = 0;
                biClrUsed       = 0;
                biClrImportant  = 0;
        } else {
        memcpy(&biWidth, &bj_buff[bj_buff_index], 4);
        bj_buff_index += 4;
                //biWidth               = SDL_ReadLE32(src);
                
        memcpy(&biHeight, &bj_buff[bj_buff_index], 4);
        bj_buff_index += 4;
        //biHeight      = SDL_ReadLE32(src);
                
        memcpy(&biPlanes, &bj_buff[bj_buff_index], 2);
        bj_buff_index += 2;
        //biPlanes      = SDL_ReadLE16(src);
                
        memcpy(&biBitCount, &bj_buff[bj_buff_index], 2);
        bj_buff_index += 2;
        //biBitCount    = SDL_ReadLE16(src);
                
        memcpy(&biCompression, &bj_buff[bj_buff_index], 4);
        bj_buff_index += 4;
        //biCompression = SDL_ReadLE32(src);
                
        memcpy(&biSizeImage, &bj_buff[bj_buff_index], 4);
        bj_buff_index += 4;
        //biSizeImage   = SDL_ReadLE32(src);
                
        memcpy(&biXPelsPerMeter, &bj_buff[bj_buff_index], 4);
        bj_buff_index += 4;
        //biXPelsPerMeter       = SDL_ReadLE32(src);
                
        memcpy(&biYPelsPerMeter, &bj_buff[bj_buff_index], 4);
        bj_buff_index += 4;
        //biYPelsPerMeter       = SDL_ReadLE32(src);
                
        memcpy(&biClrUsed, &bj_buff[bj_buff_index], 4);
        bj_buff_index += 4;
        //biClrUsed     = SDL_ReadLE32(src);
                
        memcpy(&biClrImportant, &bj_buff[bj_buff_index], 4);
        bj_buff_index += 4;
        //biClrImportant        = SDL_ReadLE32(src);
        }

        /* stop some compiler warnings. */
        (void) bfSize;
        (void) bfReserved1;
        (void) bfReserved2;
        (void) biPlanes;
        (void) biSizeImage;
        (void) biXPelsPerMeter;
        (void) biYPelsPerMeter;
        (void) biClrImportant;

        if (biHeight < 0) {
                topDown = SDL_TRUE;
                biHeight = -biHeight;
        } else {
                topDown = SDL_FALSE;
        }

        /* Check for read error */
    // bj commented out
        //if ( SDL_strcmp(SDL_GetError(), "") != 0 ) {
        //      was_error = SDL_TRUE;
        //      goto done;
        //}

        /* Expand 1 and 4 bit bitmaps to 8 bits per pixel */
        switch (biBitCount) {
                case 1:
                case 4:
                        ExpandBMP = biBitCount;
                        biBitCount = 8;
                        break;
                default:
                        ExpandBMP = 0;
                        break;
        }

        /* We don't support any BMP compression right now */
        Rmask = Gmask = Bmask = 0;
        switch (biCompression) {
                case BI_RGB:
                        /* If there are no masks, use the defaults */
                        if ( bfOffBits == (14+biSize) ) {
                                /* Default values for the BMP format */
                                switch (biBitCount) {
                                        case 15:
                                        case 16:
                                                Rmask = 0x7C00;
                                                Gmask = 0x03E0;
                                                Bmask = 0x001F;
                                                break;
                                        case 24:
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
                                                Rmask = 0x000000FF;
                                                Gmask = 0x0000FF00;
                                                Bmask = 0x00FF0000;
                                                break;
#endif
                                        case 32:
                                                Rmask = 0x00FF0000;
                                                Gmask = 0x0000FF00;
                                                Bmask = 0x000000FF;
                                                break;
                                        default:
                                                break;
                                }
                                break;
                        }
                        /* Fall through -- read the RGB masks */

                case BI_BITFIELDS:
                        switch (biBitCount) {
                                case 15:
                                case 16:
                                case 32:
                    memcpy(&Rmask, &bj_buff[bj_buff_index], 4);
                    bj_buff_index += 4;
                                        //Rmask = SDL_ReadLE32(src);
                    
                    memcpy(&Gmask, &bj_buff[bj_buff_index], 4);
                    bj_buff_index += 4;
                                        //Gmask = SDL_ReadLE32(src);
                                        
                    memcpy(&Bmask, &bj_buff[bj_buff_index], 4);
                    bj_buff_index += 4;
                    //Bmask = SDL_ReadLE32(src);
                                        break;
                                default:
                                        break;
                        }
                        break;
                default:
                        //SDL_SetError("Compressed BMP files not supported");
            SDL_DebugPrint("Compressed BMP files not supported");
                        was_error = SDL_TRUE;
                        goto done;
        }

        /* Create a compatible surface, note that the colors are RGB ordered */
        surface = SDL_CreateRGBSurface(SDL_SWSURFACE,
                        biWidth, biHeight, biBitCount, Rmask, Gmask, Bmask, 0);
        //surface = SDL_CreateRGBSurface(SDL_SWSURFACE, biWidth, biHeight, 16, 0x001F, 0x3E0, 0x7C00, 0);
        surface->pitch = biWidth*(surface->format->BytesPerPixel); // 2 bytes (for 16bit, should be 1 byte for 8bit)
        printf("    surface bytes per pixel, bitbitcount: %d, %d\n", surface->format->BytesPerPixel, biBitCount);
        if ( surface == NULL ) {
                was_error = SDL_TRUE;
                goto done;
        }

        /* Load the palette, if any */
        palette = (surface->format)->palette;
        if ( palette ) {
        printf("  Using palette, bits per pixel: %d\n", biBitCount);
                if ( biClrUsed == 0 ) {
                        biClrUsed = 1 << biBitCount;
                }
                if ( biSize == 12 ) {
                        for ( i = 0; i < (int)biClrUsed; ++i ) {
                palette->colors[i].b = bj_buff[bj_buff_index];
                bj_buff_index++;
                                //SDL_RWread(src, &palette->colors[i].b, 1, 1);
                
                palette->colors[i].g = bj_buff[bj_buff_index];
                bj_buff_index++;
                                //SDL_RWread(src, &palette->colors[i].g, 1, 1);
                                
                palette->colors[i].r = bj_buff[bj_buff_index];
                bj_buff_index++;
                //SDL_RWread(src, &palette->colors[i].r, 1, 1);
                                palette->colors[i].unused = 0;
                        }       
                } else {
                        for ( i = 0; i < (int)biClrUsed; ++i ) {
                palette->colors[i].b = bj_buff[bj_buff_index];
                bj_buff_index++;
                //SDL_RWread(src, &palette->colors[i].b, 1, 1);
                                
                palette->colors[i].g = bj_buff[bj_buff_index];
                bj_buff_index++;
                //SDL_RWread(src, &palette->colors[i].g, 1, 1);
                                
                palette->colors[i].r = bj_buff[bj_buff_index];
                bj_buff_index++;
                //SDL_RWread(src, &palette->colors[i].r, 1, 1);
                                
                palette->colors[i].unused = bj_buff[bj_buff_index];
                bj_buff_index++;
                //SDL_RWread(src, &palette->colors[i].unused, 1, 1);
                        }       
                }
                palette->ncolors = biClrUsed;
        }
        
        if (biBitCount == 16) {
            printf("16bit! using bj function!\n");
            load_bmp(src, (Uint16*)surface->pixels, biWidth, biHeight);
            goto done;
        }

        /* Read the surface pixels.  Note that the bmp image is upside down */
        //bj_buff_index = fp_offset+bfOffBits;
        bj_buff_index = bfOffBits;
        //if ( SDL_RWseek(src, fp_offset+bfOffBits, RW_SEEK_SET) < 0 ) {
        //      SDL_Error(SDL_EFSEEK);
        //      was_error = SDL_TRUE;
        //      goto done;
        //}
        top = (Uint8 *)surface->pixels;
        end = (Uint8 *)surface->pixels+(surface->h*surface->pitch);
        switch (ExpandBMP) {
                case 1:
                        bmpPitch = (biWidth + 7) >> 3;
                        pad  = (((bmpPitch)%4) ? (4-((bmpPitch)%4)) : 0);
                        break;
                case 4:
                        bmpPitch = (biWidth + 1) >> 1;
                        pad  = (((bmpPitch)%4) ? (4-((bmpPitch)%4)) : 0);
                        break;
                default:
                        pad  = ((surface->pitch%4) ?
                                        (4-(surface->pitch%4)) : 0);
            printf("  PAD: %d\n", pad);
                        break;
        }
        if ( topDown ) {
                bits = top;
        } else {
                bits = end - surface->pitch;
        }
        // bj test
        //SDL_RWseek(src, bj_buff_index, RW_SEEK_SET);
        while ( bits >= top && bits < end ) {
                switch (ExpandBMP) {
                        case 1:
                        case 4: {
                        Uint8 pixel = 0;
                        int   shift = (8-ExpandBMP);
                        for ( i=0; i<surface->w; ++i ) {
                                if ( i%(8/ExpandBMP) == 0 ) {
                    //printf("In expand BMP!\n");
                                        pixel |= bj_buff[bj_buff_index];
                    bj_buff_index++;
                    //if ( !SDL_RWread(src, &pixel, 1, 1) ) {
                                                //SDL_SetError(
                                            //"Error reading from BMP");
                    //    SDL_DebugPrint("Error reading from BMP");
                                        //      was_error = SDL_TRUE;
                                        //      goto done;
                                        //}
                    //bj_buff_index++;
                                }
                                *(bits+i) = (pixel>>shift);
                                pixel <<= ExpandBMP;
                        } }
                        break;

                        default:
                        memcpy(bits, &bj_buff[bj_buff_index], surface->pitch);
                        bj_buff_index += surface->pitch;
                        //if ( SDL_RWread(src, bits, 1, surface->pitch)
                        //                               != surface->pitch ) {
                                //SDL_Error(SDL_EFREAD);
                        //      was_error = SDL_TRUE;
                        //      goto done;
                        //}

// bj test

            /*if (biBitCount == 15 || biBitCount == 16) {
                Uint16 *pix = (Uint16 *)bits;
                Uint16 cur_pix;
                                for(i = 0; i < surface->w; i++)
                                            //pix[i] = SDL_Swap16(pix[i]);
                        cur_pix = pix[i];
                        pix[i] =  gs_rgb_to_psx(((cur_pix>>10)&31), ((cur_pix>>6)&31), (cur_pix&31));
                        //pix[i] = (cur_pix<<10) | cur_pix | (cur_pix >> 11);
            }*/
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
                        /* Byte-swap the pixels if needed. Note that the 24bpp
                           case has already been taken care of above. */
                        switch(biBitCount) {
                                case 15:
                                case 16: {
                    printf("In biBitcount == 16!\n");
                                        Uint16 *pix = (Uint16 *)bits;
                                        for(i = 0; i < surface->w; i++)
                                                pix[i] = SDL_Swap16(pix[i]);
                                        break;
                                }

                                case 32: {
                                        Uint32 *pix = (Uint32 *)bits;
                                        for(i = 0; i < surface->w; i++)
                                                pix[i] = SDL_Swap32(pix[i]);
                                        break;
                                }
                        }
#endif
                        break;
                }
                /* Skip padding bytes, ugh */
                if ( pad ) {
                        //printf("In pad: %d\n", pad);
                        Uint8 padbyte;
                        for ( i=0; i<pad; ++i ) {
                            padbyte = bj_buff[bj_buff_index];
                            bj_buff_index++;
                                //SDL_RWread(src, &padbyte, 1, 1);
                        }
                }
                if ( topDown ) {
                        bits += surface->pitch;
                } else {
                        bits -= surface->pitch;
                }
        }
done:
        if ( was_error ) {
                if ( src ) {
                        SDL_RWseek(src, fp_offset, RW_SEEK_SET);
                }
                if ( surface ) {
                        SDL_FreeSurface(surface);
                }
                surface = NULL;
        }

    else {
        /* Create a PSX Sprite to use for this surface */
        SpriteKey *spr = &sdl_sprites[sdl_surface_count];
        
        /*
        * See if our new texture fits in the current vram position
        */
        if (vram_tex_x + surface->w > 1024) {
            vram_tex_x = S_WIDTH;
            vram_tex_y = 256;

            vram_tpage_x = vram_tex_x/64;
            vram_tpage_y = 1;
        }

        /* Center on the screen with offset */
        spr->sprite.x = 0;
        spr->sprite.y = 0;
        spr->sprite.w = surface->w; 
        spr->sprite.h = surface->h;
        spr->sprite.u = spr->sprite.v = 0;
        spr->sprite.cx = spr->sprite.cy = 0;

        spr->sprite.r = spr->sprite.g = spr->sprite.b = NORMAL_LUMINOSITY;

        spr->sprite.scalex = spr->sprite.scaley = 1;
        spr->sprite.mx = spr->sprite.my = 0;

        spr->sprite.attribute = COLORMODE(COLORMODE_16BPP);

        // Set the new calculated tpage of the sprite
        spr->sprite.tpage = vram_tpage_x+(vram_tpage_y*16); 
        printf("set sprite tpage: %d\n", vram_tpage_x+(vram_tpage_y*16));

        // set the surface pointer that maps to the GsSprite
        sdl_sprites[sdl_surface_count].surface = surface;

        printf("Setting spr surface: 0x%X\n", spr->surface);

        if (biBitCount == 8) {

            /* If the image doesn't have an even width we need to pad it
             * this is because we're uploading to aligned 2 bytes/16 bits 
             */
            if (surface->w & 1) {
                Uint8 *test_pixels = (Uint8*)malloc((surface->w+1)*surface->h);
                for (i=0; i<surface->w; ++i) {
                    for (j=0; j<surface->h; ++j) {
                        test_pixels[j*(surface->w+1)+i] = ((Uint8*)surface->pixels)[j*surface->w+i];
                    }
                }

                // upload as 'half' the width since each pixel is 1byte instead of 2 as psx expects
                LoadImage(test_pixels, vram_tex_x, vram_tex_y, (surface->w+1)>>1, surface->h);

                free(test_pixels);

                vram_tex_x += (surface->w+1)/2;
            } else {
                LoadImage(surface->pixels, vram_tex_x, vram_tex_y, (surface->w)>>1, surface->h);
                vram_tex_x += surface->w/2;
            }

            //while(GsIsDrawing());

            printf("Loading palette!\n");
            Uint16 *psx_pal = (Uint16*)malloc(biClrUsed*sizeof(Uint16));
            for (i=0; i<biClrUsed; ++i) {
                psx_pal[i] = gs_rgb_to_psx((palette->colors[i].r), (palette->colors[i].g), (palette->colors[i].b));
            }

            LoadImage(psx_pal, vram_clut_x, vram_clut_y, biClrUsed, 1);
            vram_clut_y++;

            spr->sprite.attribute = COLORMODE(COLORMODE_8BPP);
            spr->sprite.cx = vram_clut_x;
            spr->sprite.cy = vram_clut_y-1;

            spr->sprite.w = surface->w;

            free(psx_pal);

            /*
            * Update the vram texture locations
            */
            printf("Vram tex x before, tpage x, width: %d,%d,%d\n", vram_tex_x, vram_tpage_x, surface->w);
            //vram_tex_x += (surface->w)/2;

        } else {

            // switch pixels
            // this is done in bj load_bmp now
            /*for (i=0; i<surface->w; ++i) {
                for (j = 0; j < surface->h; ++j) {
                    Uint16 curpix = ((Uint16*)surface->pixels)[j*surface->w+i];

                    uint8_t r = (curpix >> 10) & 31;
                    uint8_t g = (curpix >> 5) & 31;
                    uint8_t b = curpix & 31;

                    ((Uint16*)surface->pixels)[j*surface->w+i] = (b<<10) | (g<<5) | r;
                }
            }*/

            // upload the image to vram
            //LoadImage(pixbuf, vram_tex_x, vram_tex_y, width, height);
            LoadImage(surface->pixels, vram_tex_x, vram_tex_y, surface->w, surface->h);
            printf("VRam tex x, y: %d, %d\n", vram_tex_x, vram_tex_y);
            // should we call while(GsIsDrawing()) here?

            /*
            * Update the vram texture locations
            */
            printf("Vram tex x before, tpage x, width: %d,%d,%d\n", vram_tex_x, vram_tpage_x, surface->w);
            vram_tex_x += surface->w;
        }

        //vram_tex_x += width%64;
        if (vram_tex_x % 64) vram_tex_x += 64 - (vram_tex_x % 64); // texture pages must be 64pixel aligned
        //vram_tpage_x += (width / 64) + ((width % 64) ? 2 : 0); // this is possibly incorrec
        vram_tpage_x = vram_tex_x / 64; // this should theoretically always line up...
        printf("vram_tex_x after, tpage: %d\n", vram_tex_x, vram_tpage_x);

        // increase the total number of surfaces
        sdl_surface_count++;
        printf("Surface Count: %d\n", sdl_surface_count);
    }

        if ( freesrc && src ) {
                SDL_RWclose(src);
        }
        return(surface);
}


