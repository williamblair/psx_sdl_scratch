#include "PSX_SDL.h"

void SDL_Error(SDL_errorcode code)
{
	char message[100];
	sprintf(message, "SDL Error Code Set: %d\n", (int) code);
	SDL_DebugPrint(message);
}
