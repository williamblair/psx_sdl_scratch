/*
 * Sprite.h
 */

#include "SDL.h"

#ifndef _SPRITE_H_INCLUDED_
#define _SPRITE_H_INCLUDED_

typedef struct {
    SDL_Surface *surface;
    SDL_Rect clip_rect;
    SDL_Rect draw_rect;
} Sprite;

int load_sprite(Sprite *spr, const char *fname);
int draw_sprite(Sprite *spr, SDL_Surface *surface);

#define set_sprite_w(spr, width) \
    (spr)->draw_rect.w = (spr)->clip_rect.w = (width)

#define set_sprite_h(spr, height) \
    (spr)->draw_rect.h = (spr)->clip_rect.h = (height)

#define set_sprite_x(spr, xpos) \
    (spr)->draw_rect.x = (xpos)

#define set_sprite_y(spr, ypos) \
    (spr)->draw_rect.y = (ypos)

#define set_sprite_clip_x(spr, xpos) \
    (spr)->clip_rect.x = (xpos)

#define set_sprite_clip_y(spr, ypos) \
    (spr)->clip_rect.y = (ypos)

#endif // _SPRITE_H_INCLUDED_

