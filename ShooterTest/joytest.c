#include "joytest.h"

/*
 * Variables from main 
 */
extern SDL_Event    event;
extern SDL_Surface *win_surface;
extern SDL_Joystick *joy1;

extern Mix_Music *bg_music;
extern Mix_Music *drummy_music;
extern Mix_Music *dark_music;
//Mix_Music *lazer_music = NULL;
extern Mix_Chunk *lazer_chunk;
extern Mix_Chunk *hit_chunk;
extern Mix_Chunk *victory_chunk;

struct button {
    SDL_Rect rect;
    Uint32 color;
};

struct button buttons[12] = {
    {{226, 190, 24, 24}, 0}, // 0 = x
    {{260, 138, 24, 24}, 0},// 1 = circle
    {{192, 138, 24, 24}, 0},// 2 = square
    {{226,  96, 24, 24}, 0},// 3 = triangle
    {{62, 66, 24, 12}, 0},// 4 = L1
    {{227, 66, 24, 12}, 0},// 5 = R1
    {{140, 150, 12, 6}, 0},// select = 6
    {{170, 150, 12, 6}, 0},// start = 7
    {{130, 184, 12, 12}, 0},// L3 = 8
    {{180, 184, 12, 12}, 0},// R3 = 9
    {{63, 50, 24, 12}, 0},// L2 = 10
    {{228, 50, 24, 12}, 0},// R2 = 11
};

struct button pads[4] = {
    {{54, 96, 24, 24}, 0},// up
    {{54, 190, 24, 24}, 0},// down
    {{20, 138, 24, 24}, 0},// left
    {{90, 138, 24, 24}, 0}// right
};

void joystick_demo(void)
{
    int i;
    int done = 0;
    
    /* Set colors of each initially */
    for (i=0; i<12; i++)
    {
        buttons[i].color = SDL_MapRGB(win_surface->format, 150,150,150);
    }
    
    for (i=0; i<4; i++)
    {
        pads[i].color = SDL_MapRGB(win_surface->format, 150,150,150);
    }
    
    do
    {
        while(SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT) done = 1;
            else if (event.type == SDL_KEYDOWN) done = 1;
            
            else if (event.type == SDL_JOYBUTTONDOWN)
            {
                #if 0
                switch (event.jbutton.button)
                {
                    // x
                    case 0:
                        printf("X main down event!\n");
                        break;
                    // square
                    case 2:
                        printf("Square main down event!\n");
                        break;
                    // triangle
                    case 3:
                        printf("Triangle main down event!\n");
                        break;
                    // circle
                    case 1:
                        printf("Circle main down event!\n");
                        break;
                    // L1
                    case 4:
                        printf("L1 main down event!\n");
                        break;
                    // L2
                    case 10:
                        printf("L2 main down event!\n");
                        break;
                    // R1
                    case 5:
                        printf("R1 main down event!\n");
                        break;
                    // R2
                    case 11:
                        printf("R2 main down event!\n");
                        break;
                    // Select
                    case 6:
                        printf("Select main down event!\n");
                        done = 1;
                        break;
                    // Start
                    case 7:
                        printf("Select start down event!\n");
                        done = 1;
                        break;
                    // L3
                    case 8:
                        printf("L3 down event!\n");
                        break;
                    // R3
                    case 9:
                        printf("R3 down event!\n");
                        break;
                    default:
                        printf("Unhandled button!\n");
                        break;
                }
                #endif
                
                if (event.jbutton.button == 6 || event.jbutton.button == 7) {
                    done = 1;
                }
                else {
                    if (event.jbutton.button == 0) {
                        printf("MAIN: 0/X event!\n");
                    }
                    buttons[event.jbutton.button].color = SDL_MapRGB(win_surface->format, 255, 0, 0);
                }
            }
            else if (event.type == SDL_JOYBUTTONUP)
            {
                #if 0
                switch (event.jbutton.button)
                {
                    // x
                    case 0:
                        printf("X main down event!\n");
                        break;
                    // square
                    case 2:
                        printf("Square main down event!\n");
                        break;
                    // triangle
                    case 3:
                        printf("Triangle main down event!\n");
                        break;
                    // circle
                    case 1:
                        printf("Circle main down event!\n");
                        break;
                    // L1
                    case 4:
                        printf("L1 main down event!\n");
                        break;
                    // L2
                    case 10:
                        printf("L2 main down event!\n");
                        break;
                    // R1
                    case 5:
                        printf("R1 main down event!\n");
                        break;
                    // R2
                    case 11:
                        printf("R2 main down event!\n");
                        break;
                    // Select
                    case 6:
                        printf("Select main down event!\n");
                        break;
                    // Start
                    case 7:
                        printf("Select start down event!\n");
                        done = 1;
                        break;
                    // L3
                    case 8:
                        printf("L3 down event!\n");
                        break;
                    // R3
                    case 9:
                        printf("R3 down event!\n");
                        break;
                    default:
                        printf("Unhandled button!\n");
                        break;
                }
                #endif
                buttons[event.jbutton.button].color = SDL_MapRGB(win_surface->format, 150, 150, 150);
            }
            
            else if (event.type == SDL_JOYHATMOTION) 
            {
                Uint32 red = SDL_MapRGB(win_surface->format, 255, 0, 0);
                switch (event.jhat.value)
                {
                    case SDL_HAT_RIGHTUP: pads[0].color = red; pads[3].color = red; break;
                    case SDL_HAT_RIGHTDOWN: pads[1].color = red; pads[3].color = red; break;
                    case SDL_HAT_LEFTUP:pads[0].color = red; pads[2].color = red;  break;
                    case SDL_HAT_LEFTDOWN: pads[1].color = red; pads[2].color = red; break;
                    case SDL_HAT_UP: pads[0].color = red; break;
                    case SDL_HAT_DOWN: pads[1].color = red; break;
                    case SDL_HAT_LEFT: pads[2].color = red; break;
                    case SDL_HAT_RIGHT: pads[3].color = red; break;
                    default:
                        for (i=0; i<4; i++) {
                            pads[i].color = SDL_MapRGB(win_surface->format, 150,150,150);
                        }
                        break;
                }
            }
        }
        
        /* Clear the screen */
        SDL_FillRect(win_surface, NULL, SDL_MapRGB(win_surface->format, 0,0,0));
        
        /* Draw the rects */
        for (i=0; i<12; i++)
        {
            SDL_FillRect(win_surface, &buttons[i].rect, buttons[i].color);
        }
        for (i=0; i<4; i++)
        {
            SDL_FillRect(win_surface, &pads[i].rect, pads[i].color);
        }
        
        /* Update the screen */
        SDL_Flip(win_surface);
        
        SDL_Delay(1000/60);
        
    } while(!done);
}
