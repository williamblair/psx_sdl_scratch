#ifndef SOUNDTEST_H_INCLUDED
#define SOUNDTEST_H_INCLUDED

#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>

#include "SDL.h"
#include "SDL_mixer.h"

#include "Sprite.h"
#include "BMP_Font.h"
#include "Collision.h"
#include "Map.h"

/*
 * Our demo
 */
void sound_demo(void);

 #endif // SOUNDTEST_H_INCLUDED
 
 