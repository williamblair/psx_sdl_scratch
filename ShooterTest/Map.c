/*
 * Map.c
 */

#include "Map.h"

static int tile_data[] = {
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};
static Sprite tile_sprites;

void init_map(Map *map, int width, int height)
{
    map->sprites = &tile_sprites;
    map->data = tile_data;
    map->width = width/32;
    map->height = height/32;

    load_sprite(&tile_sprites, 
#ifndef PSX_BUILD
        "grass.bmp"
#else
        "cdrom:\\grass.bmp;1"
#endif
    );
    set_sprite_clip_x(&tile_sprites, 0); set_sprite_clip_y(&tile_sprites, 0);
    set_sprite_x(&tile_sprites, 0); set_sprite_y(&tile_sprites, 0);
    set_sprite_w(&tile_sprites, 32); set_sprite_h(&tile_sprites, 32);
}

void draw_map(Map *map, SDL_Surface *screen)
{
    Sprite *spr_ptr;
    int i, j, tile;
    for (i = 0; i < map->width; ++i)
    {
        for (j = 0; j < map->height; ++j)
        {
            tile = map->data[j*map->width + i];
            spr_ptr = &(map->sprites[tile]);

            set_sprite_x(spr_ptr, i*(spr_ptr->draw_rect.w));
            set_sprite_y(spr_ptr, j*(spr_ptr->draw_rect.h));
            draw_sprite(spr_ptr, screen);
        }
    }
    //spr_ptr = &(map->sprites[0]);
    //printf("About to draw tile sprite, surface: 0x%X\n", spr_ptr->surface);
    //draw_sprite(spr_ptr, screen);
}
