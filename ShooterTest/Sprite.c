/*
 * Sprite.c
 */
#include "Sprite.h"

int load_sprite(Sprite *spr, const char *fname)
{
    spr->surface = SDL_LoadBMP(fname);
    if (!spr->surface) {
        printf("Error creating surface: %s\n", SDL_GetError());
        return -1;
    }

    return 1;
}

int draw_sprite(Sprite *spr, SDL_Surface *surface) 
{
    if (SDL_BlitSurface(spr->surface, &(spr->clip_rect), surface, &(spr->draw_rect)) < 0) {
    	printf("Error Blitting Surface: %s\n", SDL_GetError());
        return -1;
    }

    return 1;
}

