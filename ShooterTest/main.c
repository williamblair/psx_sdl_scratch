/*
 * main.c
 */

#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>

#include "SDL.h"
#include "SDL_mixer.h"

#include "Sprite.h"
#include "BMP_Font.h"
#include "Collision.h"
#include "Map.h"

#include "joytest.h"
#include "soundtest.h"

#ifndef PSX_BUILD
#define S_WIDTH  640
#define S_HEIGHT 480
#else
#define USE_JOYSTICK
#define S_WIDTH  320
#define S_HEIGHT 240
#endif

// dumb SDL thing
#ifdef __WIN32__
#undef main
#endif

// using joystick
#define USE_JOYSTICK

SDL_Event    event;
SDL_Surface *win_surface = NULL;
SDL_Joystick *joy1 = NULL;

Mix_Music *bg_music = NULL;
Mix_Music *drummy_music = NULL;
Mix_Music *dark_music = NULL;
//Mix_Music *lazer_music = NULL;
Mix_Chunk *lazer_chunk = NULL;
Mix_Chunk *hit_chunk = NULL;
Mix_Chunk *victory_chunk = NULL;

typedef struct {
    Sprite body;
    Sprite flame;

    int flame_clip_x[2];
    int cur_flame_clip;
    int anim_delay;
    int anim_counter;
    
    int health;
} Ship;

typedef struct {
    Sprite eye;
    Sprite body;

    int eye_clip_x[2];
    int eye_clip_y[2];
    int cur_eye_clip;
    int anim_delay;
    int anim_counter;
    
    int health;
} Alien;

typedef struct {
    SDL_Rect rect;
    int is_drawn;
} Bullet;

typedef struct {
    Sprite body;
    Sprite pipe;
    
    int pipe_clip_y[2];
    int cur_pipe_clip;
    int anim_delay;
    int anim_counter;
} Astro;

Sprite win;
Sprite lose;
Sprite sad;
Sprite disco;
Sprite planets;
Sprite lazer;
Ship ship;
Alien alien;
Astro astro;
static int num_bullets = 0; // how many bullets are moving currently
#define TOTAL_BULLETS 10
Bullet bullets[TOTAL_BULLETS]; // arbitrary number

int exit_application = 0; // to test at the end of loop to quit the game

#define NUM_STARS 20
typedef struct {
    SDL_Rect rect;
    int speed;
    int frame_count;
} Star;
Star stars[NUM_STARS];

static Uint32 healthbar_color;

void draw_ship()
{
    draw_sprite(&ship.body, win_surface);
    draw_sprite(&ship.flame, win_surface);
}

void animate_ship()
{
    ship.anim_counter++;
    if (ship.anim_counter == ship.anim_delay) {
        ship.cur_flame_clip = !ship.cur_flame_clip;
        set_sprite_clip_x(&ship.flame, ship.flame_clip_x[ship.cur_flame_clip]);
        ship.anim_counter = 0;
    }
}

void draw_alien()
{
    draw_sprite(&alien.body, win_surface);
    draw_sprite(&alien.eye, win_surface);
    
    // check for collision with player
    if (isCollision(&ship.body.draw_rect, &alien.body.draw_rect)) {
        ship.health--;
        healthbar_color = SDL_MapRGB(win_surface->format, 255, 255, 0);
        Mix_PlayChannel(-1, hit_chunk, 0);
    } else {
        healthbar_color = SDL_MapRGB(win_surface->format, 0, 255, 0);
    }
}

void draw_healthbar()
{
    // alien's
    SDL_Rect healthbar;
    healthbar.x = 115;
    healthbar.y = 10;
    healthbar.w = (S_WIDTH-120) * ((alien.health)/100.0);
    healthbar.h = 10;
    
    SDL_FillRect(win_surface, &healthbar, SDL_MapRGB(win_surface->format, 255, 0, 0));
    
    draw_text(win_surface, "ALIEN HEALTH:", 5, 10);
    
    // space ships
    healthbar.x = 80;
    healthbar.y = S_HEIGHT - 20;
    healthbar.w = (S_WIDTH - 100) * ((ship.health)/1000.0);
    healthbar.h = 8;
    
    //SDL_FillRect(win_surface, &healthbar, SDL_MapRGB(win_surface->format, 0, 255, 0));
    SDL_FillRect(win_surface, &healthbar, healthbar_color);
}

void move_alien(int amount_x, int amount_y) {
    alien.body.draw_rect.x += amount_x;
    alien.body.draw_rect.y += amount_y;

    alien.eye.draw_rect.x += amount_x;
    alien.eye.draw_rect.y += amount_y;
}

void animate_alien()
{
static int pos_counter = 0;
/* Indices for eye positions */
static int positions[5][2] = {
    {0,0}, {1,0}, {0,1}, {1,0}, {0,0}
};

/* For a movement path */
static int movement_pos[2];
static int move_direction[2]; 
move_direction[0] = alien.body.draw_rect.x > movement_pos[0] ? -1 : 1;
move_direction[1] = alien.body.draw_rect.y > movement_pos[1] ? -1 : 1;

static int done_moving[2] = {0,0}; // flag for when we're done moving to the specified location

/* Positions the alien will move between */
static int target_locations[3][2] = {
    {10, 10},
    {300, 10},
    {100, 300}
};
    /* Set a new random location to move to */
    if (done_moving[0] && done_moving[1]) {
        int targ_index = rand()%3;

        printf("Selected new target index: %d\n", targ_index);

        movement_pos[0] = target_locations[targ_index][0];
        movement_pos[1] = target_locations[targ_index][1];

        done_moving[0] = 0; done_moving[1] = 0;


        move_direction[0] = alien.body.draw_rect.x > movement_pos[0] ? -1 : 1;
        move_direction[1] = alien.body.draw_rect.y > movement_pos[1] ? -1 : 1;
    }

    /* Sprite sheet animation */
    alien.anim_counter++;
    if (alien.anim_counter == alien.anim_delay) {
        pos_counter++;
        if (pos_counter == 5) pos_counter = 0;

        set_sprite_clip_x(&alien.eye, alien.eye_clip_x[positions[pos_counter][0]]);
        set_sprite_clip_y(&alien.eye, alien.eye_clip_y[positions[pos_counter][1]]);

        alien.anim_counter = 0;
    }

    /* Movement animation */
    if (alien.body.draw_rect.x != movement_pos[0]) {
        //alien.body.draw_rect.x += move_direction[0];
        move_alien(move_direction[0], 0);
    } else {
        done_moving[0] = 1;
    }
    if (alien.body.draw_rect.y != movement_pos[1]) {
        //alien.body.draw_rect.y += move_direction[1];
        move_alien(0, move_direction[1]);
    } else {
        done_moving[1] = 1;
    }

}

void draw_bullets()
{
    int i;
    for (i=0; i<TOTAL_BULLETS; ++i) {
        if (bullets[i].is_drawn) {
            
            bullets[i].rect.y -= 5;
            
            //SDL_FillRect(win_surface, &bullets[i].rect, SDL_MapRGB(
            //    win_surface->format, 255, 0, 0)
            //);
            set_sprite_x(&lazer, bullets[i].rect.x);
            set_sprite_y(&lazer, bullets[i].rect.y);
            draw_sprite(&lazer, win_surface);
            
            // collision
            if (isCollision(&bullets[i].rect, &alien.eye.draw_rect)) {
                printf("Hit Eye!\n");
                bullets[i].is_drawn = 0;
                
                Mix_PlayChannel(-1, hit_chunk, 0);
                
                alien.health--;
                if (alien.health == 0) {
                    printf("Alien Health Zero! Win!\n");
                }
            }
            // left screen
            else if (bullets[i].rect.y < 0) {
                bullets[i].is_drawn = 0;
            }
        }
    }
}

/*
 * Init SDL and create a window
 */
int init(void)
{
    if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_JOYSTICK) < 0) {
        printf("Error initializing SDL: %s\n", SDL_GetError());
        return -1;
    }

    if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096) < 0) {
        printf("Error opening audio: %s\n", Mix_GetError());
        return -1;
    }

    if (!(win_surface = SDL_SetVideoMode(S_WIDTH, S_HEIGHT, 32, SDL_HWSURFACE|SDL_RESIZABLE))) {
        printf("Error creating window surface: %s\n", SDL_GetError());
        return -1;
    }

#ifdef USE_JOYSTICK
    if (!(joy1 = SDL_JoystickOpen(0))) {
        printf("Error opening joystick: %s\n", SDL_GetError());
        return -1;
    }
#endif
    // test...
    //SDL_WM_ToggleFullScreen(win_surface);

    return 0;
}

/* Initialize star positions and sizes*/
void init_stars(void)
{
    int i, size;
    for (i=0; i<NUM_STARS; ++i)
    {
        size = 1+rand()%1;
        stars[i].rect.w = size;
        stars[i].rect.h = size;

        stars[i].rect.x = rand()%S_WIDTH;
        stars[i].rect.y = rand()%S_HEIGHT;

        stars[i].speed = rand()%5;
        stars[i].frame_count = 0;
    }
}

void draw_stars(void)
{
    int i;
    for (i=0; i<NUM_STARS; ++i)
    {
        stars[i].frame_count++;

        if (stars[i].frame_count > stars[i].speed)
        {
            stars[i].rect.y++;
            stars[i].frame_count = 0;
            
            //printf("Star frame count > speed!\n");

            if (stars[i].rect.y >= S_HEIGHT-stars[i].rect.h)
            {
                //printf("Resetting star position\n");

                //stars[i].rect.x = rand()%S_WIDTH;
                stars[i].rect.y = 1;
                //stars[i].speed = rand() % 10;
            }
        }
        SDL_FillRect(win_surface, &(stars[i].rect), SDL_MapRGB(win_surface->format, 255, 255, 255));
    }
}

void draw_astro(void)
{
    draw_sprite(&astro.body, win_surface);
    draw_sprite(&astro.pipe, win_surface);
    
    astro.anim_counter++;
    if (astro.anim_counter > astro.anim_delay) {
        astro.anim_counter = 0;
        astro.anim_delay = rand()%300;
        
        astro.cur_pipe_clip = !astro.cur_pipe_clip;
        set_sprite_clip_y(&astro.pipe, astro.pipe_clip_y[astro.cur_pipe_clip]);
    }
}

/*
 * Sprites and such
 */
int load_images(void)
{
    if (load_font(
#ifndef PSX_BUILD
        "fontpix.bmp"
#else
        "cdrom:\\fontpix.bmp;1"
#endif
    ) < 0) return -1;

    if (load_sprite(&ship.body, 
#ifndef PSX_BUILD
    "ship.bmp"
#else
    "cdrom:\\ship.bmp;1"
#endif
        ) < 0) return -1;
    SDL_SetColorKey(ship.body.surface, SDL_SRCCOLORKEY, SDL_MapRGB(win_surface->format, 0,0,0));

    set_sprite_clip_x(&ship.body, 0); set_sprite_clip_y(&ship.body, 0);
    set_sprite_x(&ship.body, 0); set_sprite_y(&ship.body, 0);
    set_sprite_w(&ship.body, 32); set_sprite_h(&ship.body, 32);

    // we're using the same spritesheet for the flame
    ship.flame.surface = ship.body.surface;
    set_sprite_x(&ship.flame, ship.body.draw_rect.x + 12);
    set_sprite_y(&ship.flame, ship.body.draw_rect.y + 32);
    set_sprite_w(&ship.flame, 7);
    set_sprite_h(&ship.flame, 11);
    ship.flame_clip_x[0] = 0; ship.flame_clip_x[1] = 25;
    ship.cur_flame_clip = 0;
    set_sprite_clip_x(&ship.flame, ship.flame_clip_x[ship.cur_flame_clip]);
    set_sprite_clip_y(&ship.flame, 38);
    ship.anim_counter = 0;
    ship.anim_delay = 5;
    ship.health = 1000;

    // We're using the same spritesheet for the alien eye and body
    load_sprite(&alien.body, 
#ifndef PSX_BUILD
    "alien.bmp"
#else
    "cdrom:\\alien.bmp;1"
#endif
    );
    SDL_SetColorKey(alien.body.surface, SDL_SRCCOLORKEY, SDL_MapRGB(win_surface->format, 0,0,0));
    set_sprite_x(&alien.body, 0); set_sprite_y(&alien.body, 0);
    set_sprite_w(&alien.body, 128); set_sprite_h(&alien.body, 64);
    set_sprite_clip_x(&alien.body, 0); set_sprite_clip_y(&alien.body, 0);
    alien.eye.surface = alien.body.surface;
    alien.cur_eye_clip = 0; alien.eye_clip_x[0] = 0; alien.eye_clip_x[1] = 57;
    set_sprite_x(&alien.eye, alien.body.draw_rect.x + 36); set_sprite_y(&alien.eye, alien.body.draw_rect.y + 5);
    set_sprite_w(&alien.eye, 57); set_sprite_h(&alien.eye, 23);
    set_sprite_clip_x(&alien.eye, alien.eye_clip_x[alien.cur_eye_clip]);
    set_sprite_clip_y(&alien.eye, 64);
    alien.eye_clip_y[0] = 64;
    alien.eye_clip_y[1] = 88;
    alien.anim_counter = 0;
    alien.anim_delay = 20;
    alien.health = 100;

    load_sprite(&astro.body, 
    #ifndef PSX_BUILD
        "astro.bmp"
    #else
        "cdrom:\\astro.bmp;1"
    #endif
    );
    //set_sprite_clip_x(&astro, 0); set_sprite_clip_y(&astro, 1);
    SDL_SetColorKey(astro.body.surface, SDL_SRCCOLORKEY, SDL_MapRGB(win_surface->format, 0,0,0));
    set_sprite_x(&astro.body, 10); set_sprite_y(&astro.body, S_HEIGHT-60);
    set_sprite_w(&astro.body, 42); set_sprite_h(&astro.body, 57);
    astro.pipe.surface = astro.body.surface;
    set_sprite_x(&astro.pipe, 36); set_sprite_y(&astro.pipe, S_HEIGHT-25);
    set_sprite_w(&astro.pipe, 28); set_sprite_h(&astro.pipe, 22);
    set_sprite_clip_x(&astro.pipe, 2); set_sprite_clip_y(&astro.pipe, 60);
    astro.pipe_clip_y[0] = 60; astro.pipe_clip_y[1] = 83;
    astro.anim_counter = 0; astro.anim_delay = rand()%300;
    
    load_sprite(&planets,
#ifndef PSX_BUILD
    "planets.bmp"
#else
    "cdrom:\\planets.bmp;1"
#endif
    );
    set_sprite_x(&planets, 100);
    set_sprite_y(&planets, 100);
    set_sprite_w(&planets, 148);
    set_sprite_h(&planets, 106);
    
    load_sprite(&lazer,
#ifndef PSX_BUILD
    "lazer.bmp"
#else
    "cdrom:\\lazer.bmp;1"
#endif
    );
    set_sprite_x(&lazer, 0);
    set_sprite_y(&lazer, 0);
    set_sprite_w(&lazer, 5);
    set_sprite_h(&lazer, 10);
    SDL_SetColorKey(lazer.surface, SDL_SRCCOLORKEY, SDL_MapRGB(win_surface->format, 0,0,0));
    
    load_sprite(&disco,
#ifndef PSX_BUILD
    "disco.bmp"
#else
    "cdrom:\\disco.bmp;1"
#endif
    );
    set_sprite_w(&disco, 118); set_sprite_h(&disco, 204);
    set_sprite_x(&disco, 10); set_sprite_y(&disco, 20);
    SDL_SetColorKey(disco.surface, SDL_SRCCOLORKEY, SDL_MapRGB(win_surface->format, 0,0,0));
    
    load_sprite(&win,
#ifndef PSX_BUILD
    "win.bmp"
#else
    "cdrom:\\win.bmp;1"
#endif
    );
    set_sprite_w(&win, 200); set_sprite_h(&win, 200);
    set_sprite_x(&win, 130); set_sprite_y(&win, 20);
    SDL_SetColorKey(win.surface, SDL_SRCCOLORKEY, SDL_MapRGB(win_surface->format, 0,0,0));
    
    load_sprite(&lose,
#ifndef PSX_BUILD
    "lose.bmp"
#else
    "cdrom:\\lose.bmp;1"
#endif
    );
    set_sprite_w(&lose, 200); set_sprite_h(&lose, 200);
    set_sprite_x(&lose, 110); set_sprite_y(&lose, 20);
    SDL_SetColorKey(lose.surface, SDL_SRCCOLORKEY, SDL_MapRGB(win_surface->format, 0,0,0));
    
    if (load_sprite(&sad, 
#ifndef PSX_BUILD
    "sad.bmp"
#else
    "cdrom:\\sad.bmp;1"
#endif
        ) < 0) return -1;
    SDL_SetColorKey(sad.surface, SDL_SRCCOLORKEY, SDL_MapRGB(win_surface->format, 0,0,0));
    set_sprite_w(&sad, 79); set_sprite_h(&sad, 100);
    set_sprite_x(&sad, 10); set_sprite_y(&sad, 40);
    
    return 0;
}

/*
 * Sounds
 */
int load_music(void)
{
    if (!(bg_music = Mix_LoadMUS(
#ifndef PSX_BUILD
            "song.wav"
#else
            "cdrom:\\song.raw;1"
#endif
            ))) {
        printf("Error loading music: %s\n", Mix_GetError());
        return -1;
    }
    
    if (!(drummy_music = Mix_LoadMUS(
#ifndef PSX_BUILD
            "drummy.wav"
#else
            "cdrom:\\drummy.raw;1"
#endif
            ))) {
        printf("Error loading music: %s\n", Mix_GetError());
        return -1;
    }
    
        if (!(dark_music = Mix_LoadMUS(
#ifndef PSX_BUILD
            "dark.wav"
#else
            "cdrom:\\dark.raw;1"
#endif
            ))) {
        printf("Error loading music: %s\n", Mix_GetError());
        return -1;
    }

    if (!(lazer_chunk = Mix_LoadWAV(
#ifndef PSX_BUILD
        "lazerq.wav"
#else
        "cdrom:\\lazerq.raw;1"
#endif
        ))) {
        printf("Error loading music: %s\n", Mix_GetError());
        return -1;
    }
    
    if (!(hit_chunk = Mix_LoadWAV(
#ifndef PSX_BUILD
        "hit.wav"
#else
        "cdrom:\\hit.raw;1"
#endif
        ))) {
        printf("Error loading music: %s\n", Mix_GetError());
        return -1;
    }
    
    if (!(victory_chunk = Mix_LoadWAV(
#ifndef PSX_BUILD
        "victory.wav"
#else
        "cdrom:\\victory.raw;1"
#endif
    ))) {
        printf("Error loading music: %s\n", Mix_GetError());
        return -1;
    }
    
    return 0;
}

/*
 * Blit surfaces
 */
void draw(void)
{
    /* Current player's score */
    static char score_text[100];

    /* Clear the screen */
    SDL_FillRect(win_surface, NULL, SDL_MapRGB(win_surface->format, 0,0,0));

    /* Draw the map */
    //draw_map(&map, win_surface);

    draw_stars();

    draw_alien();

    //draw_sprite(&ship.body, win_surface);
    draw_ship();
    
    draw_bullets();
    
    draw_healthbar();

    draw_astro();
    
    //sprintf(score_text, "Score: %d", score);
    //draw_text(win_surface, score_text, S_WIDTH-200, 10);

    //SDL_FillRect(win_surface, &rect1, SDL_MapRGB(win_surface->format, 255, 0, 0));
    //SDL_FillRect(win_surface, &rect2, SDL_MapRGB(win_surface->format, 0, 255, 0));

    /* Update the screen */
    SDL_Flip(win_surface);
}

void handle_events(void)
{
    static const int speed = 1; // player speed

#ifndef PSX_BUILD
    Uint8 *key = SDL_GetKeyState(NULL);

    if (key[SDLK_UP]) {
        //printf("Up key!\n");
        ship.body.draw_rect.y -= speed;
    }
    else if (key[SDLK_DOWN]) {
        //printf("Down key!\n");
        ship.body.draw_rect.y += speed;
    }
    if (key[SDLK_LEFT]) {
        ship.body.draw_rect.x -= speed;
    }
    else if (key[SDLK_RIGHT]) {
        ship.body.draw_rect.x += speed;
    }
#endif

#ifdef USE_JOYSTICK
    //SDL_JoystickUpdate();
    Uint8 hat = SDL_JoystickGetHat(joy1, 0);
    switch (hat)
    {
        case SDL_HAT_LEFTUP: ship.body.draw_rect.x -= speed; ship.body.draw_rect.y -= speed; break;
        case SDL_HAT_LEFTDOWN: ship.body.draw_rect.x -= speed; ship.body.draw_rect.y += speed; break;
        case SDL_HAT_RIGHTUP: ship.body.draw_rect.x += speed; ship.body.draw_rect.y -= speed; break;
        case SDL_HAT_RIGHTDOWN: ship.body.draw_rect.x += speed; ship.body.draw_rect.y += speed; break;
        
        case SDL_HAT_LEFT: ship.body.draw_rect.x -= speed; break;
        case SDL_HAT_RIGHT: ship.body.draw_rect.x += speed; break;
        case SDL_HAT_UP: ship.body.draw_rect.y -= speed; break;
        case SDL_HAT_DOWN: ship.body.draw_rect.y += speed; break;
        default:
            break;
    }
#endif
    /* Check for collision with the stick to pick up */
    /*if (isCollision(&ship.body.draw_rect, &rect1)) {
        printf("Picked up stick!\n");
        score += 1;
        stick_x = rand() % map.width;
        stick_y = rand() % map.height;

        rect1.x = stick_x * map.sprites[0].draw_rect.w + 3;
        rect1.y = stick_y * map.sprites[0].draw_rect.h + 3;
    }*/

    /* Keep the flame in the correct location */
    set_sprite_x(&ship.flame, ship.body.draw_rect.x + 12);
    set_sprite_y(&ship.flame, ship.body.draw_rect.y + 32);
}

void start_loop_draw(void)
{
    static int planets_dir_x = 1;
    static int planets_dir_y = 1;
    static int delay=10;
    static int delay_count=0;
    draw_stars();
    
    draw_sprite(&planets, win_surface);
    
    delay_count++;
    if (delay_count < delay) return;
    
    delay_count = 0;
    
    planets.draw_rect.x += planets_dir_x;
    planets.draw_rect.y += planets_dir_y;
    
    if (planets.draw_rect.x < 1) {
        planets_dir_x = 1;
    } else if (planets.draw_rect.x + planets.draw_rect.w >= S_WIDTH-1) {
        planets_dir_x = -1;
    }
    if (planets.draw_rect.y < 1) {
        planets_dir_y = 1;
    } else if (planets.draw_rect.y + planets.draw_rect.h >= S_HEIGHT-1) {
        planets_dir_y = -1;
    }
}

void start_loop(void)
{
    Uint8 done = 0;
    
    alien.health = 100;
    ship.health = 1000;
    
    Mix_HaltMusic();
    Mix_PlayMusic(bg_music, -1);
    
    do
    {
        while(SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT) done = 1;
            else if (event.type == SDL_KEYDOWN) done = 1;
            #ifdef USE_JOYSTICK
            else if (event.type == SDL_JOYBUTTONDOWN) done = 1;
            #endif
        }
        
        SDL_FillRect(win_surface, NULL, SDL_MapRGB(win_surface->format, 0,0,0));
        
        start_loop_draw();

        SDL_Flip(win_surface);
        
        /* TODO - more accurate FPS handling */
        SDL_Delay(1000/60);
    } while(!done);
}

void victory_draw()
{
    draw_stars();
    
    draw_sprite(&disco, win_surface);
    
    draw_sprite(&win, win_surface);
}

void lose_draw()
{
    //SDL_FillRect(win_surface, NULL, SDL_MapRGB(win_surface->format, 100,100,0));
    
    draw_stars();
    draw_sprite(&sad, win_surface);
    draw_sprite(&lose, win_surface);
}

void main_loop(void)
{
    void (*end_loop)(void);
    
    printf("In main loop!\n");
    Uint8 done = 0;
    do
    {
        while(SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT) done =  1;

            else if (event.type == SDL_KEYDOWN) {
                switch (event.key.keysym.sym)
                {
                    case SDLK_ESCAPE: done = 1; break;
                    case SDLK_SPACE:
                        bullets[num_bullets].is_drawn = 1;
                        bullets[num_bullets].rect.w = 5;
                        bullets[num_bullets].rect.h = 10;
                        bullets[num_bullets].rect.x = ship.body.draw_rect.x + 12;
                        bullets[num_bullets].rect.y = ship.body.draw_rect.y - 5;
                        num_bullets = (num_bullets + 1)%TOTAL_BULLETS;
                        //Mix_PlayMusic(lazer_music, 1);
                        Mix_PlayChannel(-1, lazer_chunk, 0);
                        break;
                    default: break;
                }
            }
#ifdef USE_JOYSTICK
            if (event.type == SDL_JOYBUTTONDOWN) {
                printf("Joybuttondown, main: %d\n", event.jbutton.button);
                
                switch (event.jbutton.button)
                {
                    // x
                    case 0:
                        printf("X main down event!\n");
                        bullets[num_bullets].is_drawn = 1;
                        bullets[num_bullets].rect.w = 5;
                        bullets[num_bullets].rect.h = 5;
                        bullets[num_bullets].rect.x = ship.body.draw_rect.x + 12;
                        bullets[num_bullets].rect.y = ship.body.draw_rect.y - 5;
                        num_bullets = (num_bullets + 1)%TOTAL_BULLETS;
                        //Mix_PlayMusic(lazer_music, 1);
                        Mix_PlayChannel(-1, lazer_chunk, 0);
                        break;
                    // square
                    case 2:
                        printf("Square main down event!\n");
                        break;
                    // triangle
                    case 3:
                        printf("Triangle main down event!\n");
                        break;
                    // circle
                    case 1:
                        printf("Circle main down event!\n");
                        break;
                    // L1
                    case 4:
                        printf("L1 main down event!\n");
                        break;
                    // L2
                    case 10:
                        printf("L2 main down event!\n");
                        break;
                    // R1
                    case 5:
                        printf("R1 main down event!\n");
                        break;
                    // R2
                    case 11:
                        printf("R2 main down event!\n");
                        break;
                    // Select
                    case 6:
                        printf("Select main down event!\n");
                        break;
                    // Start
                    case 7:
                        printf("Select start down event!\n");
                        break;
                    // L3
                    case 8:
                        printf("L3 down event!\n");
                        break;
                    // R3
                    case 9:
                        printf("R3 down event!\n");
                        break;
                    default:
                        printf("Unhandled button!\n");
                        break;
                }
            } // end if event type == JOYBUTTONDOWN
#endif
        }

        /* Stuff that doesn't happen in a poll event */
        handle_events();

        /* Animation */
        animate_ship();
        animate_alien();

        draw();

        /* TODO - more accurate FPS handling */
        SDL_Delay(1000/60);
        
        /* Test if either the alien or ship has been killed */
        if (alien.health <= 0) done = 1;
        else if (ship.health <= 0) done = 1;

    } while (!done);
    
    done = 0;
    
    /* Stop the background music */
    Mix_HaltMusic();
    
    /* See if we won or lost */
    if (alien.health <= 0) {
        end_loop = victory_draw;
        Mix_PlayChannel(-1, victory_chunk, 0);
        Mix_PlayMusic(drummy_music, -1);
    } else {
        end_loop = lose_draw;
        Mix_PlayMusic(dark_music, -1);
    }
    
    do
    {
        while(SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT) done =  1;

            else if (event.type == SDL_KEYDOWN) {
                switch (event.key.keysym.sym)
                {
                    case SDLK_ESCAPE: exit_application = 1; done = 1; break;
                    default: break;
                }
            }
#ifdef USE_JOYSTICK
            else if (event.type == SDL_JOYBUTTONDOWN) {
                done = 1;
            }
#endif
        }
        
        SDL_FillRect(win_surface, NULL, SDL_MapRGB(win_surface->format, 0,0,0));
        
        /* Draw stuff for either lose or victory */
        end_loop();
        
        SDL_Flip(win_surface);
        
        SDL_Delay(1000/60);
        
    } while (!done);
    
}

static int menu_selection = 0; // which function we're selecting
static char *menu_options[] = {
    "CONTROLLER DEMO",
    "SOUND DEMO", 
    "SHOOTER GAME"
};
void menu_sys(void)
{
    int i, y = 20, x = 10;
    int done = 0;
    SDL_Rect select_rect = {0, 0, 10, 10};
    do
    {
        /* Check for controller selection */
        while(SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT) {
                done = 1;
                exit_application = 1;
            }
            
            else if (event.type == SDL_JOYHATMOTION)
            {
                switch (event.jhat.value)
                {
                    case SDL_HAT_UP:
                        if (menu_selection > 0) menu_selection--;
                        break;
                    case SDL_HAT_DOWN:
                        if (menu_selection < 2) menu_selection++;
                        break;
                    default:
                        break;
                }
            }
            
            else if (event.type == SDL_JOYBUTTONDOWN) 
            {
                switch (event.jbutton.button)
                {
                    // x
                    case 0:
                        done = 1;
                        break;
                    default:
                        break;
                }
            }
        }
        
        /* Clear screen */
        SDL_FillRect(win_surface, NULL, 0);
        
        /* Draw our menu */
        for (i=0, y=20; i<3; i++, y += 20)
        {
            /* Draw a rect next to the current selection */
            select_rect.x = x;
            select_rect.y = y;
            if (i == menu_selection) {
                SDL_FillRect(win_surface, &select_rect, SDL_MapRGB(win_surface->format, 0, 0, 255));
            } /*else {
                SDL_FillRect(win_surface, &select_rect, SDL_MapRGB(win_surface->format, 0, 0, 0));
            }*/
            
            /* Draw the text */
            draw_text(win_surface, menu_options[i], x + 15, y);
        }
        
        /* Update the screen */
        SDL_Flip(win_surface);
        
        /* Delay */
        SDL_Delay(1000/60);
        
    } while(!done);
}

int main(void)
{
    if (init() < 0)        goto close_application;
    if (load_images() < 0) goto close_application;
    if (load_music() < 0)  goto close_application;

    init_stars();
    
    healthbar_color = SDL_MapRGB(win_surface->format, 0,255,0);
    

    do {
        menu_sys();
        if (!exit_application) 
        {
            switch (menu_selection)
            {
                // joystick demo
                case 0:
                    joystick_demo();
                    break;
                // sound demo
                case 1:
                    sound_demo();
                    break;
                // alien shooter
                default:
                    start_loop();
                    main_loop();
                    Mix_HaltMusic();
                    break;
            }
        }
    } while (!exit_application);

close_application:
    //SDL_WM_ToggleFullScreen(win_surface);
    if (win_surface != NULL) SDL_FreeSurface(win_surface);
    if (ship.body.surface != NULL) SDL_FreeSurface(ship.body.surface);
    //if (ship.flame.surface != NULL) SDL_FreeSurface(ship.flame.surface);
    if (alien.body.surface != NULL) SDL_FreeSurface(alien.body.surface);
    if (astro.body.surface != NULL) SDL_FreeSurface(astro.body.surface);
    if (lazer.surface != NULL) SDL_FreeSurface(lazer.surface);
    if (win.surface != NULL) SDL_FreeSurface(win.surface);
    if (lose.surface != NULL) SDL_FreeSurface(lose.surface);
    if (disco.surface != NULL) SDL_FreeSurface(disco.surface);
    if (sad.surface != NULL) SDL_FreeSurface(sad.surface);
//#ifndef PSX_BUILD
    if (bg_music != NULL) Mix_FreeMusic(bg_music);
    if (lazer_chunk != NULL) Mix_FreeChunk(lazer_chunk);
    if (hit_chunk != NULL) Mix_FreeChunk(hit_chunk);
//#endif
    Mix_CloseAudio();
    SDL_Quit();
    return 0;
}

