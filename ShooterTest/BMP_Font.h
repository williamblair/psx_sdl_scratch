/*
 * BMP Font.h
 */

#ifndef _BMP_FONT_H_
#define _BMP_FONT_H_

#include "SDL.h"
#include <stdio.h>

/*
 * These functions are both specific to the 'font.bmp'
 * In this directory, would need modification for other images...
 */
int load_font(const char *filename);
int draw_text(SDL_Surface *screen, const char *message, int x, int y);

#endif

